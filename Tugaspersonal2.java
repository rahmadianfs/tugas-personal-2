/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.tugaspersonal2;
import java.util.Scanner;
/**
 *
 * @author rahma
 */
public class Tugaspersonal2 {
    
   static int Factorial(int n)
    {
       if(n == 1)
         return 1;
       return  n*Factorial(n-1);
    }

    public static void main(String[] args)  {
        boolean cek = false, cek2=false;
        boolean ulang = true;
        int banyakangka, b, i;
        int a=1, total=0;
        String repeat;
     
        Scanner input = new Scanner(System.in);
       
        while(ulang){
        System.out.println("Belajar Deret Aritmatika, Geometri dan menghitung Faktorial");
        
        do {
        System.out.print("Masukkan banyak angka yang mau dicetak [2..10] : ");
        banyakangka = input.nextInt();
        if(banyakangka < 2 || banyakangka > 10)
        {
            System.out.println("Angka yang diinput tidak sesuai"); 
        }else{
             cek=true;
             }
          } while(!cek);
        
        do{
            System.out.print("Masukkan beda masing-masing angka [2..9] : ");
            b = input.nextInt();
        if (b < 2 || b > 9)
        {
            System.out.println("Angka yang diinput tidak sesuai");
        }else{
            cek2=true;
             }
          } while(!cek2);
        
        //Deret Aritmatika
        System.out.println("Deret Aritmatika : ");
        for(i=1; i<=banyakangka * b; i = i + b){
            System.out.print(i + " ");
        }
        
        //Deret Geometri
        System.out.println("\nDeret Geometri : ");
        for(i=1; i<=banyakangka; i++){
            System.out.print(a + total);
            a = a * b;
            if(i<banyakangka){
               System.out.print(" "); 
            }else {
               System.out.print(" "); 
            }
        }
        
        //Faktorial
        System.out.println("\nFaktorial dari " + banyakangka);
        for (i=banyakangka; i>=1; i--){
            System.out.print(i + " ");
        }
        System.out.println("= " + Factorial(banyakangka));
        
        //Pengulangan
        System.out.print("\nAnda mau ulang [y/t] : ");
        repeat = input.next();

        if (repeat.equalsIgnoreCase ("t"))
            ulang = false;
        if (repeat.equalsIgnoreCase ("y"))
            ulang = true;
   
        else if (repeat.equalsIgnoreCase("t"))
            System.out.println("\nTerimakasih");
        else

        System.exit (0);
        }
    }
}
